package org.aryalinux;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class Graph<T> {
	public static final class Vertex<T> {
		public T data;
		public List<Edge<T>> edges;

		public Vertex(T data) {
			this.data = data;
			this.edges = new ArrayList<Edge<T>>();
		}

		public String toString() {
			return "" + data;
		}
	};

	public static final class Edge<T> {
		public Double weight;
		public Vertex<T> destination;

		public Edge(Vertex<T> destination) {
			this.destination = destination;
		}

		public Edge(Double weight, Vertex<T> destination) {
			this.weight = weight;
			this.destination = destination;
		}
	}

	public static final class PathDetails<T> {
		public Double weight;
		public Vertex<T> parent;

		public PathDetails() {
			this(0.0, null);
		}

		public PathDetails(Double weight, Vertex<T> parent) {
			this.weight = weight;
			this.parent = parent;
		}

		public String toString() {
			String result = "[";
			result = result + "Weight: " + weight;
			result = result + ", Parent: " + parent + "]";
			return result;
		}

	}

	public List<Vertex<T>> vertices;

	public Graph() {
		vertices = new ArrayList<Graph.Vertex<T>>();
	}

	public Graph<T> addVertex(T data) {
		Graph.Vertex<T> vertex = new Graph.Vertex<T>(data);
		vertices.add(vertex);
		return this;
	}

	public Graph<T> addEdge(T source, T destination) {
		Vertex<T> v1 = getVertex(source);
		Vertex<T> v2 = getVertex(destination);
		if (v1 == null || v2 == null) {
			throw new RuntimeException("Cannot create edge. Vertices dont exist.");
		} else {
			v1.edges.add(new Edge<T>(v2));
		}
		return this;
	}

	public Graph<T> addEdge(T source, T destination, Double weight) {
		Vertex<T> v1 = getVertex(source);
		Vertex<T> v2 = getVertex(destination);
		if (v1 == null || v2 == null) {
			throw new RuntimeException("Cannot create edge. Vertices dont exist.");
		} else {
			v1.edges.add(new Edge<T>(weight, v2));
		}
		return this;
	}

	public Graph<T> addEdges(T source, T... destinations) {
		Vertex<T> v1 = getVertex(source);
		if (v1 == null) {
			throw new RuntimeException("Cannot create edge. Vertices dont exist.");
		}
		for (T destination : destinations) {
			Vertex<T> v2 = getVertex(destination);
			if (v2 == null) {
				throw new RuntimeException("Cannot create edge. Vertices dont exist.");
			} else {
				v1.edges.add(new Edge<T>(v2));
			}
		}
		return this;
	}

	public Vertex<T> getVertex(Object data) {
		Vertex<T> found = null;
		for (Vertex<T> v : vertices) {
			if (v.data.equals(data)) {
				found = v;
			}
		}
		return found;
	}

	public List<T> breadthFirstSearch(T start) {
		List<Vertex<T>> visited = new ArrayList<Graph.Vertex<T>>();
		Queue<Vertex<T>> queue = new Queue<Vertex<T>>();
		List<T> output = new ArrayList<T>();
		queue.queue(getVertex(start));
		visited.add(getVertex(start));
		while (true) {
			Vertex<T> v = null;
			try {
				v = queue.dequeue();
				output.add(v.data);
			} catch (Exception ex) {
				break;
			}
			for (Graph.Edge<T> edge : v.edges) {
				if (!visited.contains(edge.destination)) {
					queue.queue(edge.destination);
					visited.add(edge.destination);
				}
			}
		}
		return output;
	}

	public List<T> depthFirstSearch(T start) {
		List<Vertex<T>> visited = new ArrayList<Graph.Vertex<T>>();
		return dfs(getVertex(start), visited);
	}

	private List<T> dfs(Vertex<T> v, List<Vertex<T>> visited) {
		List<T> output = new ArrayList<T>();
		if (!visited.contains(v)) {
			output.add(v.data);
			visited.add(v);
			for (Edge<T> e : v.edges) {
				List<T> dfsAdjacent = dfs(e.destination, visited);
				for (T ref : dfsAdjacent) {
					output.add(ref);
				}
			}
			return output;
		} else {
			return new ArrayList<T>();
		}
	}

	public HashMap<Vertex<T>, PathDetails<T>> djikstrasShortestRoute(T source) {
		List<Vertex<T>> shortestPath = new ArrayList<Graph.Vertex<T>>();
		HashMap<Vertex<T>, Double> distances = new HashMap<Graph.Vertex<T>, Double>();
		HashMap<Vertex<T>, Vertex<T>> parents = new HashMap<Vertex<T>, Vertex<T>>();
		for (Vertex<T> vertex : vertices) {
			if (vertex.data == source) {
				distances.put(vertex, 0.0);
				parents.put(vertex, vertex);
			} else {
				distances.put(vertex, Double.POSITIVE_INFINITY);
			}
		}
		while (shortestPath.size() != vertices.size()) {
			Vertex<T> shortestDistanceVertex = getShortestDistanceVertex(distances, shortestPath);
			shortestPath.add(shortestDistanceVertex);
			Double currentDistance = distances.get(shortestDistanceVertex);
			for (Edge<T> edge : shortestDistanceVertex.edges) {
				Double distance = edge.weight + currentDistance;
				if (distances.get(edge.destination) > distance) {
					distances.put(edge.destination, distance);
					parents.put(edge.destination, shortestDistanceVertex);
				}
			}
		}
		HashMap<Vertex<T>, PathDetails<T>> result = new HashMap<Graph.Vertex<T>, Graph.PathDetails<T>>();
		for (Vertex<T> vertex : distances.keySet()) {
			PathDetails<T> pathDetails = new PathDetails<T>(distances.get(vertex), parents.get(vertex));
			result.put(vertex, pathDetails);
		}
		return result;
	}

	/*
	 * We can reduce the complexity of the algorithm here by using a priority queue
	 * here instead of a List
	 */
	private Vertex<T> getShortestDistanceVertex(Map<Vertex<T>, Double> distances, List<Vertex<T>> shortestPath) {
		Double minimum = Double.POSITIVE_INFINITY;
		Vertex<T> shortestVertex = null;
		for (Entry<Vertex<T>, Double> entry : distances.entrySet()) {
			if (entry.getValue() < minimum && !shortestPath.contains(entry.getKey())) {
				minimum = entry.getValue();
				shortestVertex = entry.getKey();
			}
		}
		return shortestVertex;
	}

	public Map<Vertex<T>, Vertex<T>> getMinimumSpanningTreePrimsAlgorithm(T source) {
		Map<Vertex<T>, Double> weights = new HashMap<Graph.Vertex<T>, Double>();
		Map<Vertex<T>, Vertex<T>> parents = new HashMap<Graph.Vertex<T>, Vertex<T>>();
		List<Vertex<T>> mspList = new ArrayList<Graph.Vertex<T>>();
		for (Vertex<T> vertex : vertices) {
			if (vertex.data == source) {
				weights.put(vertex, 0.0);
			} else {
				weights.put(vertex, Double.POSITIVE_INFINITY);
			}
			parents.put(vertex, null);
		}
		while (null != findShortestNotInMinimumSpanningTree(weights, mspList)) {
			Vertex<T> shortest = findShortestNotInMinimumSpanningTree(weights, mspList);
			mspList.add(shortest);
			for (Edge<T> edge : shortest.edges) {
				if (!weights.containsKey(edge.destination) || weights.get(edge.destination) > edge.weight) {
					weights.put(edge.destination, edge.weight);
					parents.put(edge.destination, shortest);
				}
			}
		}
		return parents;
	}

	/*
	 * We can reduce the complexity of the algorithm here by using a priority queue
	 * here instead of a List
	 */
	private Vertex<T> findShortestNotInMinimumSpanningTree(Map<Vertex<T>, Double> weights, List<Vertex<T>> mspList) {
		Vertex<T> min = null;
		Double weight = Double.POSITIVE_INFINITY;
		for (Entry<Vertex<T>, Double> entry : weights.entrySet()) {
			if (entry.getValue() < weight && !mspList.contains(entry.getKey())) {
				weight = entry.getValue();
				min = entry.getKey();
			}
		}
		return min;
	}
}
