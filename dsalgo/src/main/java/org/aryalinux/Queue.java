package org.aryalinux;

import java.util.ArrayList;
import java.util.List;

class Queue<T> {
	public static final class Node<T> {
		public T data;
		public Node<T> next;

		public Node(T data) {
			this.data = data;
		}
	};

	private Node<T> front;
	private Node<T> rear;

	public Queue() {
		front = null;
		rear = null;
	}

	public void queue(T data) {
		Node<T> node = new Node<T>(data);
		if (front == null) {
			front = node;
			rear = front;
		} else {
			rear.next = node;
			rear = node;
		}
	}

	public T dequeue() {
		if (front == null) {
			throw new RuntimeException("Queue empty.");
		}
		Node<T> node = front;
		front = front.next;
		return node.data;
	}

	public String toString() {
		if (front == null) {
			return "[]";
		} else {
			List<String> result = new ArrayList<String>();
			for(Node<T> current = front; current != null; current = current.next) {
				result.add(current.data.toString());
			}
			return result.toString();
		}
	}
}