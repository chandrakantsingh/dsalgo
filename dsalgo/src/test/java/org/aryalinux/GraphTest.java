package org.aryalinux;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.aryalinux.Graph.PathDetails;
import org.aryalinux.Graph.Vertex;
import org.junit.Assert;
import org.junit.Test;

public class GraphTest {
	@Test
	public void testDjikstrasAlgorithm() {
		Graph<Integer> graph = getSampleGraph();
		HashMap<Vertex<Integer>, PathDetails<Integer>> pathDetails = graph.djikstrasShortestRoute(0);
		for (Entry<Vertex<Integer>, PathDetails<Integer>> entry : pathDetails.entrySet()) {
			if (entry.getKey().data.equals(4)) {
				String actualParent = entry.getValue().parent.toString();
				String expectedParent = "5";
				Double actualWeight = entry.getValue().weight;
				Double expectedWeight = 21.0;
				boolean result = actualParent.contentEquals(expectedParent) && actualWeight.equals(expectedWeight);
				Assert.assertTrue("Result not as expected.", result);
			}
		}
	}

	@Test
	public void testPrimsAlgorithm() {
		Graph<Integer> graph = getSampleGraph();
		Map<Vertex<Integer>, Vertex<Integer>> result = graph.getMinimumSpanningTreePrimsAlgorithm(0);
		Assert.assertEquals("Parent of 5 should be 6", result.get(graph.getVertex(5)).data.intValue(), 6);
		Assert.assertEquals("Parent of 8 should be 2", result.get(graph.getVertex(8)).data.intValue(), 2);
		Assert.assertEquals("Parent of 6 should be 7", result.get(graph.getVertex(6)).data.intValue(), 7);
		Assert.assertEquals("Parent of 4 should be 3", result.get(graph.getVertex(4)).data.intValue(), 3);
		Assert.assertEquals("Parent of 1 should be 0", result.get(graph.getVertex(1)).data.intValue(), 0);
		Assert.assertEquals("Parent of 2 should be 8", result.get(graph.getVertex(2)).data.intValue(), 8);
		Assert.assertEquals("Parent of 3 should be 2", result.get(graph.getVertex(3)).data.intValue(), 2);
		Assert.assertEquals("Parent of 7 should be 6", result.get(graph.getVertex(7)).data.intValue(), 6);
	}

	private Graph<Integer> getSampleGraph() {
		Graph<Integer> graph = new Graph<Integer>();
		for (int i = 0; i < 9; i++) {
			graph.addVertex(i);
		}
		graph.addEdge(0, 1, 4.0);
		graph.addEdge(0, 7, 8.0);

		graph.addEdge(1, 2, 8.0);
		graph.addEdge(1, 7, 11.0);
		graph.addEdge(1, 0, 4.0);

		graph.addEdge(2, 1, 8.0);
		graph.addEdge(2, 3, 7.0);
		graph.addEdge(2, 5, 4.0);
		graph.addEdge(2, 8, 2.0);

		graph.addEdge(3, 4, 9.0);
		graph.addEdge(3, 5, 14.0);
		graph.addEdge(3, 2, 7.0);

		graph.addEdge(4, 3, 9.0);
		graph.addEdge(4, 5, 10.0);

		graph.addEdge(5, 4, 10.0);
		graph.addEdge(5, 3, 14.0);
		graph.addEdge(5, 2, 4.0);
		graph.addEdge(5, 6, 2.0);

		graph.addEdge(6, 5, 2.0);
		graph.addEdge(6, 7, 1.0);
		graph.addEdge(6, 8, 6.0);

		graph.addEdge(7, 0, 8.0);
		graph.addEdge(7, 1, 11.0);
		graph.addEdge(7, 6, 1.0);
		graph.addEdge(7, 8, 7.0);

		graph.addEdge(8, 2, 2.0);
		graph.addEdge(8, 6, 6.0);
		graph.addEdge(8, 7, 7.0);

		return graph;
	}
}
